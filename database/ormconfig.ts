import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { join } from 'path';

import AuthToken from 'src/modules/auth/entity/auth.entity';
import UserEntity from 'src/modules/user/entity/user.entity';

const typeORMConfig: TypeOrmModuleOptions[] = [
  {
    type: 'mysql',
    port: parseInt(process.env.DATABASE_PORT || '3306', 10),
    host: process.env.DATABASE_HOST,
    username: process.env.DATABASE_USERNAME,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME,
    synchronize: process.env.DATABASE_SYNCHRONIZE === 'true',
    dropSchema: process.env.DATABASE_DROP_SCHEMA === 'true',
    entities: [AuthToken, UserEntity],
    migrations: [join(__dirname, '..', 'database/migrations/*.{ts, js}')],
    logging: process.env.DATABASE_LOGGING === 'true',
    cli: {
      migrationsDir: 'database/migrations',
    },
  },
  {
    name: 'seed',
    type: 'mysql',
    host: process.env.DATABASE_HOST,
    port: parseInt(process.env.DATABASE_PORT || '3306', 10),
    username: process.env.DATABASE_USERNAME,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME,
    synchronize: process.env.DATABASE_SYNCHRONIZE === 'true',
    dropSchema: process.env.DATABASE_DROP_SCHEMA === 'true',
    logging: process.env.DATABASE_LOGGING === 'true',
    migrations: [join(__dirname, '..', 'database/seeds/*.{ts, js}')],
    cli: {
      migrationsDir: 'database/seeds',
    },
  },
];

export = typeORMConfig;
