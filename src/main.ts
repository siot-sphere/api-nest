import { NestFactory } from '@nestjs/core';
import * as dotenv from 'dotenv';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

dotenv.config();

import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const options = new DocumentBuilder()
    .setTitle('SIoT Backend API')
    .setDescription(
      'This document is for API guidance and testing. \n\n ### API Version : V2 \n\n ### Test User Info \n- **user1 (ADMIN)**: user1@test.com / pass_user11 \n- **user2**: user2@test.com / pass_user22 \n- **user3**: user3@test.com / pass_user33',
    )
    .setVersion('1.0')
    .addApiKey({
      type: 'http',
    })
    .addServer('http://localhost:3000/api')
    .setContact(
      'Nguyen Thanh',
      'https://facebook.com/thanhthanchuc',
      'thanhthanchuc@gmail.com',
    )
    .addBearerAuth()
    .build();

  const document = SwaggerModule.createDocument(app, options);

  app.setGlobalPrefix('api');
  SwaggerModule.setup('swagger', app, document);
  await app.listen(3000);
}
bootstrap();
