import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import { UserService } from 'src/modules/user/services/user.service';

@Injectable()
export class UserAuthMiddleware {
  constructor(private readonly userService: UserService) {}

  async use(req: Request, res: Response, next: NextFunction) {
    const authorization = req.header['authorization'];
    try {
      const token = authorization.slice(7, authorization.length).trimLeft();

      const result = jwt.verify(token, process.env.JWT_SECRET_KEY) as {
        _id: string;
      };

      const user = await this.userService.getById(result._id);

      if (!user || !user.isActive) {
        throw new HttpException('Unauthenticated', HttpStatus.UNAUTHORIZED);
      }

      req['state'].user = user;
      next();
    } catch (error) {
      throw new HttpException('Unauthenticated', HttpStatus.UNAUTHORIZED);
    }
  }
}

@Injectable()
export class AdminAuthMiddleware {
  constructor(private readonly userService: UserService) {}

  async use(req: Request, res: Response, next: NextFunction) {
    const authorization = req.header['authorization'];
    try {
      const token = authorization.slice(7, authorization.length).trimLeft();

      const result = jwt.verify(token, process.env.JWT_SECRET_KEY) as {
        _id: string;
      };

      const user = await this.userService.getById(result._id);

      if (!user || !user.isActive || !user.isAdmin) {
        throw new HttpException('Unauthenticated', HttpStatus.UNAUTHORIZED);
      }

      req['state'].user = user;
      next();
    } catch (error) {
      throw new HttpException('Unauthenticated', HttpStatus.UNAUTHORIZED);
    }
  }
}
