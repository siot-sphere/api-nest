import { Injectable, NestMiddleware, NotFoundException } from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';
import { UserService } from 'src/modules/user/services/user.service';

@Injectable()
export class UserInfoMiddleware implements NestMiddleware {
  constructor(private readonly userService: UserService) {}

  async use(req: Request, res: Response, next: NextFunction) {
    const { id } = req.params;
    const user = await this.userService.getById(id);
    if (!user) {
      throw new NotFoundException();
    }

    req['state'].user = user;

    next();
  }
}
