import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { UserService } from './services/user.service';
import { UserController } from './controllers/user.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import UserEntity from './entity/user.entity';
import { UserInfoMiddleware } from 'src/middlewares/user.middleware';
import { AdminAuthMiddleware } from 'src/middlewares/auth.middleware';

@Module({
  imports: [TypeOrmModule.forFeature([UserEntity])],
  providers: [UserService],
  controllers: [UserController],
  exports: [UserService],
})
export class UserModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(UserInfoMiddleware)
      .forRoutes({ path: 'users/:id', method: RequestMethod.ALL });
    consumer
      .apply(AdminAuthMiddleware)
      .forRoutes({ path: 'users/:id/activate', method: RequestMethod.POST });
  }
}
