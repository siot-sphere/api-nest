import * as Joi from 'joi';
import { NewUserInput } from '../interfaces/new-user.interface';

export const createUserSchema = Joi.object<NewUserInput>({
  email: Joi.string().email().required(),
  firstName: Joi.string().required(),
  lastName: Joi.string().required(),
  password: Joi.string().required(),
  isActive: Joi.bool(),
  isAdmin: Joi.bool(),
  reason: Joi.string(),
});
