import * as Joi from 'joi';
import { NewUserInput } from '../interfaces/new-user.interface';

export const updateUserSchema = Joi.object<NewUserInput>({
  email: Joi.string().email(),
  firstName: Joi.string(),
  lastName: Joi.string(),
  password: Joi.string(),
  isActive: Joi.bool(),
  isAdmin: Joi.bool(),
  reason: Joi.string(),
});
