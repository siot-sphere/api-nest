import { ApiProperty } from '@nestjs/swagger';
import { TableName } from 'src/utils';
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: TableName.User })
export default class UserEntity {
  @ApiProperty({
    required: true,
  })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ApiProperty({
    required: true,
  })
  @Column({ type: 'nvarchar', name: 'first_name', nullable: false })
  firstName: string;

  @ApiProperty({
    required: true,
  })
  @Column({ type: 'nvarchar', name: 'last_name', nullable: false })
  lastName: string;

  @ApiProperty({
    required: true,
  })
  @Column({
    type: 'boolean',
    name: 'is_admin',
    nullable: false,
    default: false,
  })
  isAdmin?: boolean;

  @ApiProperty({
    required: true,
  })
  @Column({
    type: 'boolean',
    name: 'is_active',
    nullable: false,
    default: false,
  })
  isActive?: boolean;

  @ApiProperty({
    required: true,
  })
  @Column({ type: 'nvarchar', name: 'reason', nullable: true })
  reason?: string;

  @ApiProperty({
    required: true,
  })
  @Column({ type: 'varchar', name: 'email', nullable: false, unique: true })
  email: string;

  @Column({ name: 'password', type: 'varchar', nullable: false, select: false })
  password: string;

  @Column({
    name: 'password_salt',
    type: 'varchar',
    nullable: false,
    select: false,
  })
  password_salt: string;

  @ApiProperty({
    required: true,
  })
  @Column({
    name: 'created_at',
    type: 'timestamp',
    nullable: false,
    default: 'CURRENT_TIMESTAMP',
  })
  createdAt?: string;

  @ApiProperty({
    required: true,
  })
  @Column({
    name: 'updated_at',
    type: 'timestamp',
    nullable: false,
    default: 'CURRENT_TIMESTAMP',
  })
  updatedAt?: string;
}
