import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { paginate } from 'nestjs-typeorm-paginate';
import { DeepPartial, Like, Repository } from 'typeorm';
import UserEntity from '../entity/user.entity';
import { NewUserInput } from '../interfaces/new-user.interface';
import { UserPaginationQuery } from '../interfaces/user.interface';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
  ) {}

  async getAllPaging(options: UserPaginationQuery) {
    const search = options.search ? '%' + options.search + '%' : undefined;

    return await paginate<UserEntity>(this.userRepository, options, {
      where: search
        ? [
            {
              email: Like(search),
            },
            {
              firstName: Like(search),
            },
            {
              lastName: Like(search),
            },
          ]
        : undefined,
    });
  }

  async getByEmail(email: string) {
    return await this.userRepository.findOne({ email });
  }

  async getById(id: string) {
    return await this.userRepository.findOne(id);
  }

  async create(user: NewUserInput) {
    const salt = await bcrypt.genSaltSync();
    const passwordHash = await bcrypt.hashSync(user.password, salt);

    return await this.userRepository.save({
      email: user.email,
      password: passwordHash,
      password_salt: salt,
      firstName: user.firstName,
      lastName: user.lastName,
    });
  }

  async update(updateUser: DeepPartial<UserEntity>, currentUser: UserEntity) {
    const newPassword = updateUser.password
      ? bcrypt.hashSync(updateUser.password, currentUser.password_salt)
      : undefined;

    const data: DeepPartial<UserEntity> = {
      ...updateUser,
      updatedAt: new Date().toISOString(),
      ...(newPassword
        ? {
            password: newPassword,
          }
        : {}),
    };

    await this.userRepository.update(currentUser.id, data);

    return {
      ...currentUser,
      ...data,
    };
  }

  async deleteById(id: string) {
    return await this.userRepository.delete(id);
  }
}
