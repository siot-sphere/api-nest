import { ApiProperty } from '@nestjs/swagger';
import { IPaginationOptions } from 'nestjs-typeorm-paginate';

export class PaginationQuery implements IPaginationOptions {
  @ApiProperty({
    name: 'limit',
    type: Number,
    required: false,
  })
  limit: number;

  @ApiProperty({
    name: 'page',
    type: Number,
    required: false,
  })
  page: number;
}

export class UserPaginationQuery extends PaginationQuery {
  @ApiProperty({
    name: 'search',
    type: String,
    required: false,
  })
  search?: string;
}
