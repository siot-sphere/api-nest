import { ApiProperty } from '@nestjs/swagger';
import UserEntity from '../entity/user.entity';

export class PaginationResponse {
  @ApiProperty({
    title: 'Total items',
    type: Number,
    required: true,
  })
  totalItems: number;

  @ApiProperty({
    title: 'Item count',
    type: Number,
    required: true,
  })
  itemCount: number;

  @ApiProperty({
    title: 'Items per page',
    type: Number,
    required: true,
  })
  itemsPerPage: number;

  @ApiProperty({
    title: 'Total page',
    type: Number,
    required: true,
  })
  totalPages: number;

  @ApiProperty({
    title: 'Current page',
    type: Number,
    required: true,
  })
  currentPage: number;
}

export class UserPaginationResponse {
  @ApiProperty({
    title: 'Metadata',
    type: PaginationResponse,
    required: true,
  })
  meta: PaginationResponse;

  @ApiProperty({
    title: 'Items',
    type: [UserEntity],
    required: true,
  })
  items: UserEntity[];
}
