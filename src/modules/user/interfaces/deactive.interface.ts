import { ApiHeader, ApiProperty } from '@nestjs/swagger';

@ApiHeader({
  name: 'data',
})
export class ToggleActivateObject {
  @ApiProperty({
    title: 'Is active',
    required: true,
  })
  isActive: boolean;
}
