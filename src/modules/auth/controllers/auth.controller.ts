import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Post,
  UsePipes,
} from '@nestjs/common';
import { UserService } from 'src/modules/user/services/user.service';
import { JoiValidationPipe } from 'src/pipes/joi-validation.pipe';
import { loginSchema } from '../schemas/login.schema';
import { AuthService } from '../services/auth.service';
import * as bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { createUserSchema } from 'src/modules/user/schemas/create-user.schema';
import { NewUserInput } from 'src/modules/user/interfaces/new-user.interface';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(
    private readonly userService: UserService,
    private readonly authService: AuthService,
  ) {}

  @Post('login')
  @UsePipes(new JoiValidationPipe(loginSchema, 'Email or password incorrect.'))
  async login(@Body('email') email, @Body('password') password) {
    const user = await this.userService.getByEmail(email);

    if (!user || !bcrypt.compareSync(password, user.password)) {
      throw new HttpException(
        'Email or password incorrect.',
        HttpStatus.BAD_REQUEST,
      );
    }

    if (!user.isActive) {
      throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED);
    }

    const accessToken = jwt.sign({ _id: user.id }, process.env.JWT_SECRET_KEY);
    const deviceSignToken = jwt.sign(
      { _id: user.id },
      process.env.JWT_SIGN_DEVICE_KEY,
    );

    return {
      accessToken,
      deviceSignToken,
      user,
    };
  }

  @Post('register')
  @UsePipes(new JoiValidationPipe(createUserSchema))
  async register(@Body() body: NewUserInput) {
    const user = await this.userService.create(body);

    const accessToken = jwt.sign({ _id: user.id }, process.env.JWT_SECRET_KEY);
    const deviceSignToken = jwt.sign(
      { _id: user.id },
      process.env.JWT_SIGN_DEVICE_KEY,
    );

    return {
      accessToken,
      deviceSignToken,
      user,
    };
  }

  @Get('me')
  async me() {}
}
