import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'auth_tokens' })
export default class AuthToken {
  @PrimaryGeneratedColumn('increment')
  id?: string;

  @Column({ name: 'created_at' })
  createdAt?: string;

  @Column({ name: 'updated_at' })
  updatedAt?: string;

  @Column({ name: 'user_id' })
  userId?: string;

  @Column({ name: 'access_token' })
  accessToken: string;

  @Column({ name: 'device_token' })
  deviceToken: string;
}
