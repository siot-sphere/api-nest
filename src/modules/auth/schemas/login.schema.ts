import * as Joi from 'joi';
import { LoginInput } from '../interfaces/login.interface';

export const loginSchema = Joi.object<LoginInput>({
  email: Joi.string().email().required(),
  password: Joi.string().required(),
});
