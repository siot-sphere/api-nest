import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import UserEntity from 'src/modules/user/entity/user.entity';
import { Repository } from 'typeorm';
import { LoginInput } from '../interfaces/login.interface';

Injectable();
export class AuthService {
  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
  ) {}

  async login({ email, password }: LoginInput, user: UserEntity) {}
}
