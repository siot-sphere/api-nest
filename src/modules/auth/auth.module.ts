import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserAuthMiddleware } from 'src/middlewares/auth.middleware';
import UserEntity from '../user/entity/user.entity';
import { UserService } from '../user/services/user.service';
import { AuthController } from './controllers/auth.controller';
import AuthToken from './entity/auth.entity';
import { AuthService } from './services/auth.service';

@Module({
  imports: [TypeOrmModule.forFeature([AuthToken, UserEntity])],
  controllers: [AuthController],
  providers: [UserService, AuthService],
  exports: [TypeOrmModule],
})
export class AuthModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(UserAuthMiddleware)
      .forRoutes({ path: '/auth/me', method: RequestMethod.ALL });
  }
}
