import { PaginationInput } from './type';

export function parsePaginate<T>(
  items: T[],
  query: PaginationInput,
  totalItem: number,
) {
  return {
    items,
    totalItem,
    ...query,
  };
}
