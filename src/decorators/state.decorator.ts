import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export const State = createParamDecorator(
  (data: any, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    return data ? request['state'].data : request['state'];
  },
);
